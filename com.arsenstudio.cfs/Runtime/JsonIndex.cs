using System.IO;
using UnityEngine;

namespace Cfs
{
    public class JsonIndexReader : IndexReader
    {
        public Cfs.Index LoadIndex(string filepath)
        {
            return JsonUtility.FromJson<Cfs.Index>(File.ReadAllText(filepath));
        }
    }
}
