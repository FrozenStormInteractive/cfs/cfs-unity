using System;
using System.IO;
using System.Text;

namespace Cfs
{
    internal static class FileStreamExtensions
    {
        public static int ReadInt(this FileStream fs)
        {
            byte[] buffer = new byte[4];
            fs.Read(buffer, 0, 4);
            return (ushort)(buffer[0] + (buffer[1] << 8) + (buffer[2] << 16) + (buffer[3] << 24));
        }

        public static ushort ReadUShort(this FileStream fs)
        {
            byte[] buffer = new byte[2];
            fs.Read(buffer, 0, 2);
            return (ushort)(buffer[0] + (buffer[1] << 8));
        }

        public static string ReadGUID(this FileStream fs)
        {
            byte[] buffer = new byte[16];
            fs.Read(buffer, 0, 16);
            Guid guid = new Guid(buffer);
            return guid.ToString();
        }

        public static string ReadString(this FileStream fs)
        {
            ushort strLength = fs.ReadUShort();

            if(strLength == 0)
            {
                return null;
            }

            byte[] buffer = new byte[strLength];
            fs.Read(buffer, 0, strLength);
            return Encoding.UTF8.GetString(buffer, 0, buffer.Length);
        }
    }

    public class BinaryIndexReader : IndexReader
    {
        public Cfs.Index LoadIndex(string filepath)
        {
            Cfs.Index index = new Cfs.Index();

            using (FileStream fs = File.OpenRead(filepath))
            {
                fs.Seek(3, SeekOrigin.Begin);

                byte version = Convert.ToByte(fs.ReadByte());

                index.root = fs.ReadString();
                int containerCount = fs.ReadInt();
                index.containers = new Container[containerCount];
                int entryCount = fs.ReadInt();
                index.entries = new Entry[entryCount];

                for(int i = 0; i < containerCount; i++)
                {
                    Container cont = new Container();
                    cont.guid = fs.ReadGUID();
                    cont.path = fs.ReadString();
                    index.containers[i] = cont;
                }

                for(int i = 0; i < entryCount; i++)
                {
                    Entry entry = new Entry();
                    entry.guid = fs.ReadGUID();
                    entry.path = fs.ReadString();

                    string md5hash = fs.ReadString();
                    string sha1hash = fs.ReadString();

                    if(md5hash != null || sha1hash != null)
                    {
                        Entry.Hashes hashes = new Entry.Hashes();
                        hashes.md5 = md5hash;
                        hashes.sha1 = sha1hash;
                        entry.hash = hashes;
                    }

                    int blockCount = fs.ReadInt();
                    entry.blocks = new Entry.Block[blockCount];

                    for(int j = 0; j < blockCount; j++)
                    {
                        Entry.Block block = new Entry.Block();
                        block.container = fs.ReadGUID();
                        block.offset = fs.ReadInt();
                        block.size = fs.ReadInt();
                        entry.blocks[j] = block;
                    }

                    index.entries[i] = entry;
                }
            }

            return index;
        }
    }
}
