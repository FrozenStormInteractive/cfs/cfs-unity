using System;

namespace Cfs
{
    [Serializable]
    public class Entry
    {
        /// <summary>
        /// Entry Identifier
        /// </summary>
        public string guid;

        public string path = null;

        [Serializable]
        public class Hashes
        {
            public string md5;

            public string sha1;
        }

        public Hashes hash = null;

        [Serializable]
        public class Block
        {
            /// <summary>
            /// Container GUID
            /// </summary>
            public string container;

            /// <summary>
            /// Position of data in the container
            /// </summary>
            public int offset;

            /// <summary>
            /// Size of data
            /// </summary>
            public int size;
        }


        public Block[] blocks;
    }

    [Serializable]
    public class Container
    {
        /// <summary>
        /// Container Indentifier
        /// </summary>
        public string guid;

        /// <summary>
        /// Relative to Index filepath (or Index root if present).
        /// </summary>
        public string path;
    }

    [Serializable]
    public class Index
    {
        /// <summary>
        /// Relative to Index filepath.
        /// </summary>
        public string root = null;

        public Container[] containers;

        public Entry[] entries;
    }

    public interface IndexReader
    {
        Cfs.Index LoadIndex(string filepath);
    }
}
