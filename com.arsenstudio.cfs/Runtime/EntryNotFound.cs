using System;

namespace Cfs
{
    [Serializable]
    public class EntryNotFoundException : System.Exception
    {
        public string Entry { get; private set; }

        public EntryNotFoundException(string entry)
        {
            Entry = entry;
        }
    }

}
