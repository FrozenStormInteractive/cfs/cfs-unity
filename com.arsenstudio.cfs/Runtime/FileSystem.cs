using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Cfs
{
    public class FileSystem
    {
        [Flags]
        public enum EOptions
        {
            None = 0b_0000_0000,
            IntegrityCheck = 0b_0000_0001,
        }
        public EOptions Options = EOptions.None;

        private string m_indexPath;
        private Cfs.Index m_index;

        private Dictionary<string, int> m_entryGuidMap;
        private Dictionary<string, string> m_entryPathMap;

        private Dictionary<string, int> m_containerGuidMap;

        public static FileSystem FromJsonIndex(string indexFilePath)
        {
            Cfs.Index index = new JsonIndexReader().LoadIndex(indexFilePath);
            FileSystem fs = new FileSystem();
            fs.m_index = index;
            fs.RefreshCache();
            fs.m_indexPath = Path.GetDirectoryName(Path.GetFullPath(indexFilePath));
            return fs;
        }

        public static FileSystem FromBinaryIndex(string indexFilePath)
        {
            Cfs.Index index = new BinaryIndexReader().LoadIndex(indexFilePath);
            FileSystem fs = new FileSystem();
            fs.m_index = index;
            fs.RefreshCache();
            fs.m_indexPath = Path.GetDirectoryName(Path.GetFullPath(indexFilePath));
            return fs;
        }

        private FileSystem()
        {
        }

        private void RefreshCache()
        {
            m_entryGuidMap = new Dictionary<string, int>();
            m_entryPathMap = new Dictionary<string, string>();

            for(int i = 0; i < m_index.entries.Length; i++) {
                Entry entry = m_index.entries[i];
                m_entryGuidMap[entry.guid] = i;
                if(entry.path != null) {
                    m_entryPathMap[entry.path] = entry.guid;
                }
            }

            m_containerGuidMap = new Dictionary<string, int>();

            for(int i = 0; i < m_index.containers.Length; i++) {
                Container cont = m_index.containers[i];
                m_containerGuidMap[cont.guid] = i;
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="entry">Entry guid</param>
        /// <returns></returns>
        public byte[] GetEntryDataWithGuid(string entryGuid)
        {
            if(!m_entryGuidMap.ContainsKey(entryGuid)) {
                throw new EntryNotFoundException(entryGuid);
            }

            Entry entry = m_index.entries[m_entryGuidMap[entryGuid]];

            int totalSize = 0;
            for(int i = 0; i < entry.blocks.Length; i++)
            {
                totalSize += entry.blocks[i].size;
            }

            byte[] data = new byte[totalSize];

            for(int i = 0, offset = 0; i < entry.blocks.Length; i++)
            {
                Entry.Block block = entry.blocks[i];

                Container cont = m_index.containers[m_containerGuidMap[block.container]];

                using (var file = File.OpenRead(GetContainerFullPath(cont.path)))
                {
                    file.Seek(block.offset, SeekOrigin.Begin);
                    file.Read(data, offset, block.size);
                }

                offset += block.size;
            }

            if(entry.hash != null && Options.HasFlag(EOptions.IntegrityCheck)) {
                if(entry.hash.md5 != null) {
                    if(entry.hash.md5 != CryptoUtils.CalculateMD5Hash(data)) {
                        throw new CheckFailedException();
                    }
                }
                if(entry.hash.sha1 != null) {
                    if(entry.hash.sha1 != CryptoUtils.CalculateSHA1Hash(data)) {
                        throw new CheckFailedException();
                    }
                }
            }

            return data;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="entry">Entry path</param>
        /// <returns></returns>
        public byte[] GetEntryData(string entryPath)
        {
            if(m_entryPathMap.ContainsKey(entryPath)) {
                return GetEntryDataWithGuid(m_entryPathMap[entryPath]);
            } else {
                throw new EntryNotFoundException(entryPath);
            }
        }

        private string GetContainerFullPath(string path)
        {
            var strbuilder = new StringBuilder(m_indexPath).Append(Path.DirectorySeparatorChar);
            if(m_index.root != null) {
                strbuilder.Append(Path.DirectorySeparatorChar).Append(m_index.root);
            }

            strbuilder.Append(path);

            return strbuilder.ToString();
        }

        public bool ExistsGuid(string entryGuid)
        {
            return m_entryGuidMap.ContainsKey(entryGuid);
        }

        public bool Exists(string entryPath)
        {
            return m_entryPathMap.ContainsKey(entryPath);
        }
    }
}
