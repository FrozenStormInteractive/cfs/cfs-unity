using System;

namespace Cfs
{
    [Serializable]
    public class CheckFailedException : System.Exception
    {
        public CheckFailedException() { }
        public CheckFailedException(string message) : base(message) { }
        public CheckFailedException(string message, System.Exception inner) : base(message, inner) { }
        protected CheckFailedException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

}
