# Contributing Guidelines

Thanks for taking the time to contribute!

The following is a set of guidelines for contributing to Unity CFS. These are just guidelines, not rules, so use your best judgement and feel free to propose changes to this document in a pull request.

## Community

* The whole documentation, such as setting up a development environment, setting up, and testing, can be read [here](Documentation~/index.md).

* If you have any questions regarding Unity CFS, open an [issue on GitLab](https://gitlab.com/ArsenStudio/cfs/cfs-unity/issues/new) or [on GitHub](https://github.com/ArsenStudio/cfs-unity/issues/new).

## Issue

Ensure the bug was not already reported by searching under [issues](https://gitlab.com/ArsenStudio/cfs/cfs-unity/issues). If you're unable to find an open issue addressing the bug, open a [new issue](https://gitlab.com/ArsenStudio/cfs/cfs-unity/issues/new).

Please pay attention to the following points while opening an issue.

### Does it happen on web browsers? (especially Chrome)
Zulip's desktop client is based on Electron, which integrates the Chrome engine within a standalone application.
If the problem you encounter can be reproduced on web browsers, it may be an issue with [Zulip web app](https://github.com/zulip/zulip).

### Write detailed information
Detailed information is very helpful to understand an issue.

For example:
* How to reproduce the issue, step-by-step.
* The expected behavior (or what is wrong).
* Screenshots for GUI issues.
* The application version.
* The operating system.
* The Zulip-Desktop version.


## Pull Requests
Pull Requests are always welcome.

1. When you edit the code, please run `npm run test` to check the formatting of your code before you `git commit`.
2. Ensure the PR description clearly describes the problem and solution. It should include:
   * The operating system on which you tested.
   * The Zulip-Desktop version on which you tested.
   * The relevant issue number, if applicable.
