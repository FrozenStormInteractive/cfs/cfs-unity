# Chunked File System for Unity

CFS C# Implementation for Unity3D.

Please see the [installation guide](Documentation~/installation.md).


## Documentation

Please see the [Documentation](Documentation~/index.md)

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

Also see our [contribution guidelines](CONTRIBUTING.md).

## License

[MIT](LICENSE.txt)
