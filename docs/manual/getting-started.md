# Getting Started

You can create a filesystem with index file:
```csharp
string indexPath = "index.json";
cfs = Cfs.FileSystem.FromIndexFile(indexPath);
```

## Check file integrity

```csharp
cfs.Options |= Cfs.FileSystem.EOptions.IntegrityCheck;
```

Warning: Integrity check is resource-intensive. So it is disabled by default.

Note: Integrity check is only processed if md5 or sha1 hashes are provided in the index.

## Fetch file content

```csharp
byte[] data = cfs.GetEntryData("flower.png");

byte[] data = cfs.GetEntryDataWithGuid("529c5245-b81d-11e9-8d77-0492261e17d6");
```

## Check if a file exists

```csharp
if(cfs.Exists("flower.png"))
{
    // Do something
}

if(cfs.ExistsGuid("529c5245-b81d-11e9-8d77-0492261e17d6"))
{
    // Do something
}
```
